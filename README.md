Example scenario script for JRES 2017
----

This repository contains the script of the Grid'5000 experiment presented in JRES 2017.

The script is located in `run.py` file and requires [execo](http://execo.gforge.inria.fr/doc/latest-stable/index.html)